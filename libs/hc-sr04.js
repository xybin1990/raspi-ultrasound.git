const Gpio = require('pigpio').Gpio;
// 每ms的声速
const MICROSECDONDS_PER_CM = 1e6 / 34321;
const trigNumber = 18;
const echoNumber = 24;
const trigger = new Gpio(trigNumber, { mode: Gpio.OUTPUT });
// 设置 trigger 引脚为输出引脚
const echo = new Gpio(echoNumber, { mode: Gpio.INPUT, alert: true });
// 设置 echo 引脚为输入
trigger.digitalWrite(0);
// 设置 trigger 引脚为低电平
let cb = () => {}
const watchHCSR04 = () => {
  let startTick;
  echo.on('alert', (level, tick) => {
    if (level == 1) { startTick = tick; }
    else {
      const endTick = tick;
      const diff = (endTick >> 0) - (startTick >> 0);
      // 无符号的 32位数据
      const distance = diff / 2 / MICROSECDONDS_PER_CM;
      cb(distance)
      // process.exit(0)
    }
  });
};
watchHCSR04();
function trigHCSR04 (callback) {
  cb = callback
  trigger.trigger(10, 1)
}

module.exports = trigHCSR04;
