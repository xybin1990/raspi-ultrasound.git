# 树莓派端项目

```text

这个项目是树莓派对接爱智 Spirit1 的一个超声波测距 demo。

连接协议采用的是 CoAP。

树莓派信息如下：

Raspberry Pi 3 Model B Plus Rev 1.3
Architecture:        armv7l
Byte Order:          Little Endian
CPU(s):              4
On-line CPU(s) list: 0-3
Thread(s) per core:  1
Core(s) per socket:  4
Socket(s):           1
Vendor ID:           ARM
Model:               4
Model name:          Cortex-A53
Stepping:            r0p4
CPU max MHz:         1400.0000
CPU min MHz:         600.0000
BogoMIPS:            38.40
Flags:               half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32


pigpiod -v
79

node -v
v10.24.1

npm -v
6.14.15

树莓派端创建CoAP server，接受爱智Spirit 1 发送的测距请求。
通过调用 HC-SR04 超声波测距模块返回测量数据。

```