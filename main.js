const coap = require('coap')
const server = coap.createServer()
const trigHCSR04 = require('./libs/hc-sr04')

// the default CoAP port is 5683
// server.listen(5683, () => {
//   const req = coap.request('coap://192.168.128.1:5683/ping')

//   req.on('response', (res) => {
//     res.pipe(process.stdout)
//     console.log('>> recive <<')
//     res.on('end', () => {
//       process.exit(0)
//     })
//   })

//   req.end('vvv')
// })

const PORT = 5683
server.on('request', function (req, res) {
  console.log('>> url:', req.url.split('/'))
  console.log('>> req:', req.payload.toString())
  trigHCSR04((data) => {
    res.end(`${data}`)
  })
})

server.listen(PORT, function () {
  console.log(`>> raspi CoAP server started on port: ${PORT} <<`)
})
